package com.imam.nawawi.downloadku;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;

public class VideoActivity extends AppCompatActivity {

    private VideoView OgVideoView;
    private TextView text;
    private EditText inputLink;
    private Button btnLink, btnDownload;
    private ImageButton btnReset;
    private MediaPlayer mediaPlayer;


    String websiteTitle, websiteDescription, vidurl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        text = (TextView) findViewById(R.id.text);
        OgVideoView = (VideoView) findViewById(R.id.OgVideo); //VideoView
        inputLink = (EditText) findViewById(R.id.link);
        btnLink = (Button) findViewById(R.id.submitLink);
        btnDownload = (Button) findViewById(R.id.download);
        btnReset = (ImageButton) findViewById(R.id.btnreset);


        btnLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VideoActivity.FetchMetadataFromURL meta = new VideoActivity.FetchMetadataFromURL();
                String metaUrl = inputLink.getText().toString();
                meta.execute(metaUrl); //di kirim ke AsyncTask param "doInBackground

                Toast.makeText(VideoActivity.this, "Generate ...", Toast.LENGTH_SHORT).show();

            }
        });

        btnDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FetchMetadataFromURL meta = new FetchMetadataFromURL();

                Intent in = new Intent(getApplicationContext(),DownloadVideoActivity.class);
                in.putExtra("vidlink", vidurl.toString());
                startActivity(in);
            }
        });

        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                inputLink.setText("");
                mPStop();
            }
        });

    }

    protected void mPStop(){
        mediaPlayer.release();
        mediaPlayer.reset();
    }

    private class FetchMetadataFromURL extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... strings) {
            Document doc;

            try {
                Connection.Response response = Jsoup
                        .connect(strings[0])
                        .userAgent(
                                "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21")
                        .timeout(1000000).execute();

                int statusCode = response.statusCode();
                Log.d("TAG", " status code is: " + statusCode);
                String ogVideo = null;
                if (statusCode == 200) {
                    doc = Jsoup.connect(strings[0]).timeout(1000 * 1000).get();
                    Elements metaOgVideo = doc.select("meta[property=og:video]");

                    if (metaOgVideo != null){
                        vidurl = metaOgVideo.first().attr("content");
                        System.out.println("src :<<<------>>> " + ogVideo);
                    }

                } else {
                    System.out.println("received error code : " + statusCode);
                }
            } catch (IOException e) {
                Log.d("TAG", " Exception " + e);
                e.printStackTrace();

            }

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(Void result) {
//            text.setText("Title : " + websiteTitle + "\n\nvid Url :: " + vidurl);

            if (vidurl != null){
                Toast.makeText(VideoActivity.this, "Video Link Is Valid", Toast.LENGTH_SHORT).show();
                Uri video = Uri.parse(vidurl.toString());
                OgVideoView.setVideoURI(video);
                OgVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        mp.setLooping(true);
                        OgVideoView.start();
                    }
                });
            }else {
                Toast.makeText(VideoActivity.this, "Video Link Is not Valid", Toast.LENGTH_SHORT).show();
            }

        }
    }

}
