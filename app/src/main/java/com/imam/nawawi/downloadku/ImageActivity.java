package com.imam.nawawi.downloadku;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.squareup.picasso.Picasso;

import java.io.IOException;

public class ImageActivity extends AppCompatActivity{

    private ImageView imgOgImage;
    private TextView text;
    private EditText inputLink;
    private Button btnLink, btnDownload;
    private ImageButton btnReset;
    String websiteTitle, websiteDescription, imgurl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);
        text = (TextView) findViewById(R.id.text);
        imgOgImage = (ImageView) findViewById(R.id.imgOgImage);
        inputLink = (EditText) findViewById(R.id.link);
        btnLink = (Button) findViewById(R.id.submitLink);
        btnDownload = (Button) findViewById(R.id.download);
        btnReset = (ImageButton) findViewById(R.id.btnreset);
//        kirim pakek intent

        btnLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FetchMetadataFromURL meta = new FetchMetadataFromURL();
                String metaUrl = inputLink.getText().toString();
                meta.execute(metaUrl); //di kirim ke AsyncTask param "doInBackground

                Toast.makeText(ImageActivity.this, "Generate ...", Toast.LENGTH_SHORT).show();
            }
        });

        btnDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FetchMetadataFromURL meta = new FetchMetadataFromURL();

                Intent in = new Intent(getApplicationContext(),DownloadImageActivity.class);
                in.putExtra("imglink", imgurl.toString());
                startActivity(in);
            }
        });

        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inputLink.setText("");
                imgOgImage.setImageBitmap(null);
            }
        });
    }

    private class FetchMetadataFromURL extends AsyncTask<String, Void, Void> {


        @Override
        protected Void doInBackground(String... strings) {

            try {
                // Connect to website
                Document document = Jsoup.connect(strings[0]).get();
                // Get the html document title
                websiteTitle = document.title();



                //Here It's just print whole property of URL
                Elements metaElems = document.select("meta");
                for (Element metaElem : metaElems) {
                    String property = metaElem.attr("property");
                    Log.e("Property", "Property =" + property + " \n Value =" + metaElem.attr("content"));
                }


                // Locate the content attribute
                websiteDescription = metaElems.attr("content");
                String ogImage = null;
                Elements metaOgImage = document.select("meta[property=og:image]");
                if (metaOgImage != null) {
                    imgurl = metaOgImage.first().attr("content");
                    System.out.println("src :<<<------>>> " + ogImage);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(Void result) {
//            text.setText("Title : " + websiteTitle + "\n\nImage Url :: " + imgurl);
            //t2.setText(websiteDescription);

            if (imgurl != null){
                Picasso.with(getApplicationContext()).load(imgurl).into(imgOgImage);
                Toast.makeText(ImageActivity.this, "Image Link Is Valid", Toast.LENGTH_SHORT).show();
            }else {
                Toast.makeText(ImageActivity.this, "Image Link Is not Valid", Toast.LENGTH_SHORT).show();
            }

        }
    }
}
